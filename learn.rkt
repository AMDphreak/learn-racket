#lang slideshow
; Shapes
(define c (circle 10))
(define r (rectangle 10 20))
(define (square n)
  (filled-rectangle n n))

; Four-square. A 2x2 grid
(define (four p)
  (define two-p (hc-append item item))
  (vc-append two-p two-p))

; Checker pattern
(define (checker p1 p2)
    (let ([p12 (hc-append p1 p2)]
          [p21 (hc-append p2 p1)])
      (vc-append p12 p21)))

; Checkerboard
(define (checkerboard p)
  (let* ([rp (colorize p "red")]
         [bp (colorize p "black")]
         [c (checker rp bp)]
         [c4 (four circ)])
    (four c4)))

;
(define (series mk)
    (hc-append 4 (mk 5) (mk 10) (mk 20)))
